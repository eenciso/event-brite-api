// Instanciar Clases Interfaz y EventBrite
const eventbrite = new EventBrite();
const ui = new Interfaz();

// Listener al buscador
document.getElementById('buscarBtn').addEventListener('click', (e) => {
	e.preventDefault();
	// Leé el texto del input "buscar"
	const txtBuscador = document.getElementById('evento').value;

	// Leé el select
	const categorias = document.getElementById('listado-categorias');
	const categoriaSeleccionada = categorias.options[categorias.selectedIndex].value;

	// Validar texto del buscador
	if (txtBuscador !== '') {
		// Busqueda
		eventbrite.obtenerEventos(txtBuscador, categoriaSeleccionada)
			.then (eventos => {
				if (eventos.eventos.events.length > 0) {
					// Limpia los resultados
					ui.limpiarResultados();
					// Alerta de Resultados encontrados
					ui.mostrarEventos(eventos.eventos);
				} else {
					// Alerta de Resultados nulos
					ui.mostrarMensaje('No hay resultados', 'alert alert-danger mt-4');
				}
			})
	} else {
		// Mostrar mensaje
		ui.mostrarMensaje('Escribe algo en el buscador', 'alert alert-danger mt-4');
	}
});