class Interfaz {

	constructor () {
		// Inicializa la app al instanciar
		this.init();
		// Leer Resultado de Eventos
		this.listado = document.getElementById('resultado-eventos');
	}

	// Metodo inicialización de la API
	init () {
		// Lamado a la funcion para imprimir categorias
		this.imprimirCategorias();
	}

	// Imprimir Categorias
	imprimirCategorias () {
		const listaCategorias = eventbrite.obtenerCategorias()
			.then(categorias => {
				const cats = categorias.categorias.categories;
				// Selecciona el Select del html
				const selectCategoria = document.getElementById('listado-categorias');
				// Se recorre el arreglo y se imprimen los option
				cats.forEach( cat => {
					const option = document.createElement('option');
					option.value = cat.id;
					option.appendChild(document.createTextNode(cat.name_localized));
					selectCategoria.appendChild(option);
				})
			})
	}

	// Lee la respuesta de la Api eh imprime los Resultados
	mostrarEventos (eventos) {
		// Lee los eventos y los agrega a una variable
		const listaEventos = eventos.events;
		// Recorre los eventos y crea el template
		listaEventos.forEach(evento => {
			this.listado.innerHTML += `
				<div class="col-md-4 mb-4">
					<div class="card">
						<img class="img-fluid mb-2" src="${evento.logo !== null ? evento.logo.url : ''}" />
						<div class="card-body">
							<div class="card-text">
								<h2 class="text-center">${evento.name.text}</h2>
								<p class="lead text-info">Información del evento</p>
								<p>${evento.description.text.substring(0, 280)}...</p>
								<span class="badge badge-primary">Capacidad: ${evento.capacity}</span>
								<span class="badge badge-secondary">Fecha y Hora: ${evento.start.local}</span>
								<a href="${evento.url}" target="_blank" class="btn btn-primary btn-block mt-4">Comprar Boletos</a>
							</div>
						</div>
					</div>
				</div>
			`;
		})
	}

	// Limpia los resultados previos
	limpiarResultados () {
		this.listado.innerHTML = '';
	}

	// Metodo para imprimir mensajes | 2 parametros : mensaje y clases
	mostrarMensaje (mensaje, clases) {
		const div = document.createElement('div');
		div.classList = clases;
		div.appendChild(document.createTextNode(mensaje));
		// Selecciona el padre para insertar el div
		const buscadorDiv = document.querySelector('#buscador');
		buscadorDiv.appendChild(div);
		// Quitar alert despues de 2.5 segundos
		setTimeout( () => {
			this.limpiarMensaje();
		}, 2500);
	}

	// Eliminar mensaje
	limpiarMensaje () {
		const alert = document.querySelector('.alert');
		if (alert) {
			alert.remove();
		}
	}
}