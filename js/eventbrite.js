class EventBrite {
	constructor	() {
		this.token_auth = '';// <- Token Personal (Publico) de EventBrite https://www.eventbrite.com.mx
		this.ordenar = 'date';
	}
	// Mostrar resultados de la búsqueda
	async obtenerEventos (evento, categoria) {
		const respuestaEvento = await fetch(`https://www.eventbriteapi.com/v3/events/search/?q=${evento}&sort_by=${this.ordenar}&categories=${categoria}&token=${this.token_auth}`);
		// Espera respuesta de evento y lo devuelve como Json
		const eventos = await respuestaEvento.json();
		return {
			eventos
		}
	}

	// Obtiene las categorias en init
	async obtenerCategorias () {
		// Consultar categorias a la REST API de EvenBrite
		const respuestaCategorias = await fetch(`https://www.eventbriteapi.com/v3/categories/?token=${this.token_auth}`);
		// Esperar la respuesta de la categorias y devolver JSON
		const categorias = await respuestaCategorias.json();
		// Devolver el Resultado
		return {
			categorias
		}
	}
}