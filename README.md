
## Buscador de Eventos - Eventbrite API


Aplicacion Web creada para buscar eventos con Fecth API y Async Await, consumiendo la REST API de [Eventbrite.com](https://www.eventbrite.com)


#### Requisitos:
 - Api Key (Token) de [Eventbrite.com](https://www.eventbrite.com)

#### Instrucciones:
 - Generar una Api Key en Evenbrite
 - Colocar el Api Key al instanciar la Clase EventBrite en el archivo [***eventbrite.js***](https://gitlab.com/eenciso/event-brite-api/blob/master/js/eventbrite.js).